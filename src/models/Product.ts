export type Product = {
    id: number;
    listId: 1;
    title: string;
    quantity: number;
    price: 6.99;
}