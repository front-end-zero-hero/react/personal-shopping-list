import {createBrowserRouter, Navigate, RouterProvider} from "react-router-dom";
import App from "./App";
import React, {Component, lazy, Suspense} from "react";

const Loadable = (Component: React.FC<{}>) => (props: any) =>
(
    <Suspense>
        <Component {...props} />
    </Suspense>
)

const Lists = Loadable(lazy(() => import('./pages/Lists')));
const ListItems = Loadable(lazy(() => import('./pages/ListDetail')));
const ListForms = Loadable(lazy(() => import('./pages/ListForm')));

const router = createBrowserRouter([
    {
        path: '/',
        element: <App/>,
        children: [
            {
                path: '/',
                element: <Navigate to={'/list'}/>
            },
            {
                path: '/list',
                element: <Lists/>
            },
            {
                path: '/list/:listId',
                element: <ListItems/>
            },
            {
                path: '/list/:listId/new',
                element: <ListForms/>
            }
        ]
    }
])

const Routes = () => {
    return <RouterProvider router={router}/>
}

export default Routes;