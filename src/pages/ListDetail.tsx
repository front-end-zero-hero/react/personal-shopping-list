import styled from "styled-components";
import React, {ReactElement, useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import useDataFetching from "../hooks/useDataFetching";
import {Product} from "../models/Product";
import NavBar from "../components/NavBar/NavBar";
import ListItem from "../components/ListItem/ListItem";

const ListItemWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  margin: 2% 5%;
`;

const ListDetail: React.FC = (): ReactElement => {
    let navigate = useNavigate();
    const {listId} = useParams();

    const [loading, error, data] = useDataFetching('https://my-json-server.typicode.com/PacktPublishing/React-Projects-Second-Edition/items/');

    const[items, setItems] = useState<Product[]>([]);

    const goBack = () => navigate(-1);
    const openForm = () => navigate(`/list/${listId}//new`);

    useEffect(() => {
        data && listId && setItems(data.filter((item) => item.listId === parseInt(listId)))
    }, [data, listId])

    return (
      <>
          <NavBar
              goBack={() => navigate(-1)}
              openForm={() => navigate(`/list/${listId}//new`)}
              title={""}/>

          <ListItemWrapper>
              {loading || error ? (
                  <span>{error || 'Loading...'}</span>
              ) : (
                  items.map((item) => <ListItem key={item.id} title={item.title} quantity={item.quantity} price={item.price}/>)
              )}
          </ListItemWrapper>
      </>
    );
}

export default ListDetail;