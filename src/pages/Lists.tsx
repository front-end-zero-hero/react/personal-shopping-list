import styled from "styled-components";
import React, {ReactElement, useEffect, useState} from "react";
import {Link, useNavigate, useParams} from "react-router-dom";
import NavBar from "../components/NavBar/NavBar";
import useDataFetching from "../hooks/useDataFetching";
import {Product} from "../models/Product";

const ListWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  margin: 5%;
`;

const ListLink = styled(Link)`
  display: flex;
  text-align: left;
  align-items: center;
  padding: 1%;
  background: lightGray;
  border-radius: 5px;
  padding: 10px;
  margin-bottom: 2%;
  color: black;
  text-decoration: none;
`;

const Title = styled.h3`
  flex-basis: 80%;
`;

const ListForm: React.FC = (): ReactElement => {
    const navigate = useNavigate();
    const {listId} = useParams();

    const [loading, error, data] = useDataFetching(
        'https://my-json-server.typicode.com/PacktPublishing/React-Projects-Second-Edition/lists',
    );

    return(
        <>
            {<NavBar title='Your Lists'  goBack={undefined} openForm={undefined}/>}
            <ListWrapper>
                {loading || error ? (
                    <h1>{error || 'Loading...'}</h1>
                ) : (
                    data.map((list) => (
                        <ListLink key={list.id} to={`/list/${list.id}`}>
                            <Title>{list.title}</Title>
                        </ListLink>
                    ))
                )}

            </ListWrapper>
        </>
    );
}

export default ListForm;