import styled from "styled-components";
import React, {ReactElement} from "react";

const ListItemWrapper = styled.div`
  display: flex;
  text-align: left;
  align-items: center;
  padding: 1%;
  background: lightGray;
  border-radius: 5px;
  padding: 10px;
  margin-bottom: 2%;
  text-decoration: none;
`;

const Title = styled.h3`
  flex-basis: 70%;
`;

const Total = styled.span`
  flex-basis: 15%;
  font-weight: bold;
  text-align: right;
`;

interface IProps {
    title: string;
    quantity: number;
    price: number;
}

const ListItem: React.FC<IProps> = ({title, price, quantity}): ReactElement => {
    return (
        <ListItemWrapper>
            <Title>{title}</Title>
            <Total>{`Quantity: ${quantity}`}</Total>
            <Total>{`$: ${price}`}</Total>
        </ListItemWrapper>
    )
}

export default ListItem;