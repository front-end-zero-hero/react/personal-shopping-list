import {useEffect, useState} from "react";
import {Product} from "../models/Product";

const useDataFetching = (dataSource: string): [boolean, string, Product[]] => {
    const [loading, setLoading] = useState<boolean>(true);
    const [data, setData] = useState<Product[]>([]);
    const [error, setError] = useState<string>('');

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await fetch(dataSource);
                await wait(1000);
                const result = await data.json();

                if(result) {
                    setData(result);
                    setLoading(false);
                }
            }
            catch (e: any){
                setLoading(false);
                setError(e.message);
            }
        }

        fetchData().then();
    }, [dataSource]);

    return [loading, error, data];
}

const wait = (milliseconds: number): Promise<void> => {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

export default useDataFetching;