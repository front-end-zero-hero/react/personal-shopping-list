import React, {ReactElement} from 'react';
import './App.css';
import styled, {createGlobalStyle} from "styled-components";
import {BrowserRouter, Outlet, Route, Routes} from "react-router-dom";
import Header from "./components/Header/Header";
import Lists from "./pages/Lists";
import ListForm from "./pages/ListForm";
import ListDetail from "./pages/ListDetail";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`;

const AppWrapper = styled.div`
  text-align: center;
`;

const App: React.FC = (): ReactElement => {
  return (
      <>
        <GlobalStyle />
        <AppWrapper>
            <Header/>
            <Outlet/>
        </AppWrapper>
      </>
  );
}

export default App;
